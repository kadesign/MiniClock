package ru.kadesignlab.bukkit.miniclock;

import org.bukkit.ChatColor;

public class Constants
{
  public final static String BOARD_TITLE=ChatColor.YELLOW + (ChatColor.BOLD + "Часы");
  public final static String CLOCK_TITLE=ChatColor.BOLD + "Время:";
  public final static String DAYTIME_TITLE=ChatColor.BOLD + "Сейчас:";

  public final static String DAYTIME_MORNING="Утро";
  public final static String DAYTIME_DAY="День";
  public final static String DAYTIME_EVENING="Вечер";
  public final static String DAYTIME_NIGHT="Ночь";
}
