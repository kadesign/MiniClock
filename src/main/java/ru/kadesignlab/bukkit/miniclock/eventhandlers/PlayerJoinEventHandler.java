package ru.kadesignlab.bukkit.miniclock.eventhandlers;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import ru.kadesignlab.bukkit.miniclock.Scoreboard;
import ru.kadesignlab.bukkit.miniclock.utils.ScoreboardUtil;

public class PlayerJoinEventHandler implements Listener
{
  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent e)
  {
    ScoreboardUtil board=Scoreboard.getScoreboard();
    board.send(e.getPlayer());
  }
}
