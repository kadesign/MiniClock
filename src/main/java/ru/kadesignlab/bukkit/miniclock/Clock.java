package ru.kadesignlab.bukkit.miniclock;

import org.bukkit.Bukkit;

import static ru.kadesignlab.bukkit.miniclock.Constants.*;

public class Clock
{
  public enum DayTime
  {
    MORNING(0, DAYTIME_MORNING),
    DAY(1, DAYTIME_DAY),
    EVENING(2, DAYTIME_EVENING),
    NIGHT(3, DAYTIME_NIGHT);

    private final int id;
    private final String title;

    DayTime(int id, String title)
    {
      this.id=id;
      this.title=title;
    }

    @Override
    public String toString()
    {
      return this.title;
    }
  }

  public static String getCurrentTime()
  {
    long serverTime=Bukkit.getWorld("world").getTime();
    long hours=serverTime / 1000 + 6;
    while (hours > 23)
    {
      hours-=24L;
    }
    long minutes=(serverTime % 1000) * 60 / 1000;
    String mm="0" + minutes;
    mm=mm.substring(mm.length() - 2);

    return hours + ":" + mm;
  }

  public static DayTime getDayTime(String time)
  {
    final String[] timeArray=time.split(":");
    final int hours=Integer.parseInt(timeArray[0]);
    final int minutes=Integer.parseInt(timeArray[1]);

    if (hours <= 5)
      return DayTime.NIGHT;
    if (hours <= 11)
      return DayTime.MORNING;
    if (hours < 18 || (hours == 18 && minutes <= 29))
      return DayTime.DAY;
    return DayTime.EVENING;
  }
}
