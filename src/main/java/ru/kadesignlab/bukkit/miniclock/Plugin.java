package ru.kadesignlab.bukkit.miniclock;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.kadesignlab.bukkit.miniclock.eventhandlers.PlayerJoinEventHandler;

public class Plugin extends JavaPlugin
{
  @Override
  public void onEnable()
  {
    getServer().getPluginManager().registerEvents(new PlayerJoinEventHandler(), this);

    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
      if (Bukkit.getOnlinePlayers().size() > 0)
      {
        Scoreboard.updateScoreboard(Bukkit.getOnlinePlayers());
      }
    }, 1L, 1000L / 60L);
    getLogger().info("MiniClock is enabled");
  }

  @Override
  public void onDisable()
  {
    getLogger().info("MiniClock is disabled");
  }
}
