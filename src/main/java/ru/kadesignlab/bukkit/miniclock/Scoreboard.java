package ru.kadesignlab.bukkit.miniclock;

import org.bukkit.entity.Player;
import ru.kadesignlab.bukkit.miniclock.utils.ScoreboardUtil;

import java.util.Collection;

public class Scoreboard
{
  public static ScoreboardUtil getScoreboard()
  {
    final String time=Clock.getCurrentTime();
    ScoreboardUtil board=new ScoreboardUtil(Constants.BOARD_TITLE);

    board.add(Constants.CLOCK_TITLE);
    board.add(time);
    board.blankLine();

    board.add(Constants.DAYTIME_TITLE);
    board.add(Clock.getDayTime(time).toString());

    board.build();

    return board;
  }

  public static void updateScoreboard(Collection<? extends Player> players)
  {
    for (Player player : players)
    {
      ScoreboardUtil board=getScoreboard();
      board.send(player);
    }
  }
}
